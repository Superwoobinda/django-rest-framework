from rest_framework import serializers, exceptions

from .models import Family, Person


class PersonCreateSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Person
        fields = ["id", "name", "age", "fullname", "family", "url"]

    @staticmethod
    def validate_age(age):
        if age not in range(0, 100 + 1):
            raise exceptions.ValidationError("Person age must be in range 0 - 100 years")
        return age

    def create(self, validated_data):
        validated_data["name"] = validated_data.get("name").capitalize()
        return super().create(validated_data)


class PersonReadSerializer(PersonCreateSerializer):
    family = serializers.CharField(source="family.family_name", read_only=True)


class FamilySerializer(serializers.ModelSerializer):
    members = PersonReadSerializer(many=True, read_only=True)
    avg_family_age = serializers.IntegerField(read_only=True)

    class Meta:
        model = Family
        fields = ["id", "family_name", "members", "avg_family_age"]
