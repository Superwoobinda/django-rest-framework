from django.urls import path, include
from rest_framework import routers

from .views import FamilyViewSet, PersonViewSet

router = routers.DefaultRouter()

router.register(r"family", FamilyViewSet, base_name="family")
router.register(r"person", PersonViewSet, base_name="person")

urlpatterns = [
    path("", include(router.urls)),
]
