from rest_framework import filters


class PersonFilter(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        min_age = request.query_params.get("min_age")
        if min_age:
            queryset = queryset.filter(age__gte=min_age)

        return queryset
