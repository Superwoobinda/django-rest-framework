from django.db import models


class Family(models.Model):
    family_name = models.CharField(max_length=30, verbose_name="Family Name")

    class Meta:
        verbose_name = "Family name"

    def __str__(self):
        return self.family_name


class Person(models.Model):
    name = models.CharField(max_length=30)
    age = models.IntegerField()
    family = models.ForeignKey("Family", related_name="members", on_delete=models.CASCADE,
                               verbose_name="Family", null=True, blank=True)

    @property
    def fullname(self):
        if self.family:
            return self.name + " " + self.family.family_name
        return self.name + " " + "Snow"

    def __str__(self):
        return self.fullname
