from django.db.models import Avg
from rest_framework import viewsets
from django_filters.rest_framework import DjangoFilterBackend

from .models import Family, Person
from .serializers import FamilySerializer, PersonCreateSerializer, PersonReadSerializer
from .pagination import FamilyPaginationWithAvgPeopleAge
from .filters import PersonFilter


class FamilyViewSet(viewsets.ModelViewSet):
    queryset = Family.objects.all()
    serializer_class = FamilySerializer
    pagination_class = FamilyPaginationWithAvgPeopleAge

    def get_queryset(self):
        return self.queryset.annotate(avg_family_age=Avg('members__age'))


class PersonViewSet(viewsets.ModelViewSet):
    queryset = Person.objects.all()
    serializer_read_class = PersonReadSerializer
    serializer_create_class = PersonCreateSerializer

    filter_backends = [
        DjangoFilterBackend,
        PersonFilter
    ]

    def get_serializer_class(self):
        if self.action == "create":
            return self.serializer_create_class
        return self.serializer_read_class
