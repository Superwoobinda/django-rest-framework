from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from django.db.models import IntegerField, Avg


class FamilyPaginationWithAvgPeopleAge(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    avg_total_people_age = {}

    def get_paginated_response(self, data):
        return Response({
            'links': {
                'next': self.get_next_link(),
                'previous': self.get_previous_link()
            },
            'count': self.page.paginator.count,
            'results': data,
            'avg_total_people_age': self.avg_total_people_age,
        })

    def paginate_queryset(self, queryset, request, view=None):
        self.avg_total_people_age["avg_total_people_age"] = queryset.filter().aggregate(
            sum=Avg("avg_family_age", output_field=IntegerField())).get("sum")

        return super().paginate_queryset(queryset, request, view)
